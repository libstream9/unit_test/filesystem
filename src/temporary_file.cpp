#include <stream9/filesystem/temporary_file.hpp>

#include "data_dir.hpp"

#include <filesystem>

#include <fcntl.h>

#include <boost/test/unit_test.hpp>

#include <stream9/path/concat.hpp> // operator/

namespace testing {

using stream9::path::operator/;

namespace fs { using namespace std::filesystem; }
namespace fs { using namespace stream9::filesystem; }

static bool
is_read_write(int const fd)
{
    auto const flags = ::fcntl(fd, F_GETFL);

    return flags & O_RDWR;
}

static std::string
random_string(int const length)
{
    std::string result;

    for (auto i = 0; i < length; ++i) {
        auto const c = static_cast<char>('a' + (std::rand() % 27));
        result.push_back(c);
    }

    return result;
}

BOOST_AUTO_TEST_SUITE(temporary_file_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = fs::temporary_file;

        static_assert(std::default_initializable<T>);
        static_assert(!std::copyable<T>);
        static_assert(std::movable<T>);
        static_assert(std::destructible<T>);
        static_assert(std::totally_ordered<T>);
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        try {
            fs::path path;
            {
                fs::temporary_file f;
                path = f.path().c_str();

                BOOST_TEST(f.fd().is_open());
                BOOST_TEST(is_read_write(f.fd()));
                BOOST_TEST(path.parent_path() == fs::temp_directory_path());
            }

            BOOST_TEST(!fs::exists(path));
        }
        catch (...) {
            stream9::print_error();
        }
    }

    BOOST_AUTO_TEST_CASE(construct_with_relative_prefix_)
    {
        fs::path path;
        {
            fs::temporary_file f { "foo" };
            path = f.path().c_str();

            BOOST_TEST(f.fd().is_open());
            BOOST_TEST(is_read_write(f.fd()));
            BOOST_TEST(path.parent_path() == fs::temp_directory_path());
            BOOST_TEST(path.filename().native().starts_with("foo"));
        }

        BOOST_TEST(!fs::exists(path));
    }

    BOOST_AUTO_TEST_CASE(construct_with_absolute_prefix_)
    {
        fs::path path;
        {
            fs::temporary_file f { data_dir().string() / "foo" };
            path = f.path().c_str();

            BOOST_TEST(f.fd().is_open());
            BOOST_TEST(is_read_write(f.fd()));
            BOOST_TEST(path.is_absolute());
            BOOST_TEST(path.parent_path() == data_dir());
            BOOST_TEST(path.filename().native().starts_with("foo"));
        }

        BOOST_TEST(!fs::exists(path));
    }

    BOOST_AUTO_TEST_CASE(construct_with_bad_prefix_)
    {
        try {
            fs::path path;
            {
                auto const prefix_dir = fs::temp_directory_path() / random_string(5);
                BOOST_REQUIRE(!fs::exists(prefix_dir));

                BOOST_CHECK_EXCEPTION(
                    fs::temporary_file f { prefix_dir.string() / "foo" },
                    stream9::error,
                    [&](auto&& e) {
                        BOOST_CHECK(e.why() == stream9::filesystem::errc::fail_to_create_file);
                        return true;
                    } );
            }

            BOOST_TEST(!fs::exists(path));
        }
        catch (...) {
            stream9::print_error();
        }
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        fs::temporary_file f1 { "123" };
        fs::temporary_file f2 { "456" };

        BOOST_TEST(f1 < f2);
        BOOST_TEST(f2 > f1);
        BOOST_TEST(f1 == f1);
        BOOST_TEST(f1 != f2);
    }

    BOOST_AUTO_TEST_CASE(stream_)
    {
        fs::temporary_file f1 { "123" };

        f1 << "foo";
    }

BOOST_AUTO_TEST_SUITE_END() // temporary_file_

} // namespace testing
