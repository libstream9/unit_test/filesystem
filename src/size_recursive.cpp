#include <stream9/filesystem/size_recursive.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/cstring_ptr.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/path.hpp>

namespace testing {

using st9::cstring_ptr;
using st9::ofstream;
using st9::string_view;
using st9::path::operator/;

static void
save_file(cstring_ptr const& path, string_view contents) //TODO fs::save_file
{
    ofstream os { path };
    os << contents;
}

BOOST_AUTO_TEST_SUITE(size_recursive_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        fs::temporary_directory dir;

        auto p1 = dir.path() / "foo";

        save_file(p1, "hello");

        auto sz = fs::size_recursive(p1);

        BOOST_TEST(sz == 5);
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        fs::temporary_directory root;

        auto p1 = root.path() / "foo";
        save_file(p1, "hello");

        fs::temporary_directory subdir { root.path() / "sub" };

        auto p2 = subdir.path() / "bar";
        save_file(p2, "bye");

        auto sz = fs::size_recursive(root.path());

        BOOST_TEST(sz > 8);
    }

BOOST_AUTO_TEST_SUITE_END() // size_recursive_

} // namespace testing
