#include <stream9/filesystem/tmpfstream.hpp>

#include <filesystem>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/load_string.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/strings/starts_with.hpp>

namespace testing {

namespace fs = stream9::filesystem;
namespace str = stream9::strings;
namespace path = stream9::path;
using path_t = std::filesystem::path;

BOOST_AUTO_TEST_SUITE(tmpfstream_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        fs::tmpfstream tmp;
    }

    BOOST_AUTO_TEST_CASE(prefix_)
    {
        fs::tmpfstream tmp { "temp_" };

        auto p = tmp.path();
        BOOST_TEST(str::starts_with(path::basename(p), "temp_"));
    }

    BOOST_AUTO_TEST_CASE(stream_)
    {
        fs::tmpfstream tmp;

        tmp << "hello world" << std::flush;

        BOOST_TEST(fs::load_string(tmp.path()) == "hello world");
    }

    BOOST_AUTO_TEST_CASE(file_disappear_after_destruction_)
    {
        using std::filesystem::is_regular_file;
        using std::filesystem::exists;

        path_t p;
        {
            fs::tmpfstream tmp;
            p = tmp.path().c_str();

            BOOST_TEST(is_regular_file(p));
        }

        BOOST_TEST(!exists(p));
    }

    BOOST_AUTO_TEST_CASE(move_construct_)
    {
        fs::tmpfstream tmp1;

        tmp1 << "hello" << std::flush;

        fs::tmpfstream tmp2 { std::move(tmp1) };

        tmp2 << " world" << std::flush;

        BOOST_TEST(fs::load_string(tmp2.path()) == "hello world");
    }

    BOOST_AUTO_TEST_CASE(move_assign_)
    {
        fs::tmpfstream tmp1;

        tmp1 << "hello" << std::flush;

        fs::tmpfstream tmp2;

        tmp2 = std::move(tmp1);

        tmp2 << " world" << std::flush;

        BOOST_TEST(fs::load_string(tmp2.path()) == "hello world");
    }

BOOST_AUTO_TEST_SUITE_END() // tmpfstream_

} // namespace testing
