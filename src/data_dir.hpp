#ifndef STREAM9_XDG_TEST_DATA_DIR_HPP
#define STREAM9_XDG_TEST_DATA_DIR_HPP

#include <filesystem>

#include <boost/preprocessor/stringize.hpp>

namespace testing {

inline std::filesystem::path
data_dir()
{
    using std::filesystem::path;

    return path(BOOST_PP_STRINGIZE(DATA_DIR));
}


} // namespace testing

#endif // STREAM9_XDG_TEST_DATA_DIR_HPP
