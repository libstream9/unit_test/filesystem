#include <stream9/filesystem/load_string.hpp>

#include "data_dir.hpp"
#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(load_string_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto const& s = fs::load_string(data_dir() / "hello_world.txt");

        BOOST_TEST(s == "hello world");
    }

    BOOST_AUTO_TEST_CASE(size_limit_)
    {
        auto const& s = fs::load_string(data_dir() / "hello_world.txt", 5);

        BOOST_TEST(s == "hello");
    }

    BOOST_AUTO_TEST_CASE(file_not_found_)
    {
        using enum stream9::filesystem::errc;

        auto const& p = data_dir() / "xxx";

        BOOST_CHECK_EXCEPTION(
            fs::load_string(p),
            stream9::error,
            [](auto&& e) {
                BOOST_CHECK(e.why() == fail_to_open_file);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // load_string_

} // namespace testing
