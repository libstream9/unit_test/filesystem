#include <stream9/filesystem/exists.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::filesystem::exists;

BOOST_AUTO_TEST_SUITE(exists_)

    BOOST_AUTO_TEST_CASE(basic_)
    {
        BOOST_TEST(exists("."));
    }

BOOST_AUTO_TEST_SUITE_END() // exists_

} // namespace testing
