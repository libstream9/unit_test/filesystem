#include <stream9/filesystem/temporary_directory.hpp>

#include "namespace.hpp"

#include "data_dir.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/path/is_child_of.hpp>

namespace testing {

namespace fs { using namespace stream9::filesystem; }
namespace fs { using namespace std::filesystem; }
namespace rng = std::ranges;
namespace path = st9::path;

BOOST_AUTO_TEST_SUITE(temporary_directory_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = fs::temporary_directory;

        static_assert(std::default_initializable<T>);
        static_assert(!std::copyable<T>);
        static_assert(std::movable<T>);
        static_assert(std::destructible<T>);
        static_assert(std::totally_ordered<T>);
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        fs::path path;
        {
            fs::temporary_directory d;
            path = d.path().c_str();

            BOOST_TEST(path::is_child_of(path.c_str(), fs::temp_directory_path().c_str()));
            BOOST_TEST(fs::is_directory(path));
        }

        BOOST_TEST(!fs::exists(path));
    }

    BOOST_AUTO_TEST_CASE(construct_with_relative_prefix_)
    {
        fs::path path;
        {
            fs::temporary_directory d { "foo/bar" };
            path = d.path().c_str();

            BOOST_REQUIRE(fs::is_directory(path));
            BOOST_REQUIRE(path::is_child_of(path.c_str(), fs::temp_directory_path().c_str()));

            auto rel = fs::relative(path, fs::temp_directory_path());

            BOOST_TEST(rel.native().starts_with("foo/bar"));
        }

        BOOST_TEST(!fs::exists(path));
    }

    BOOST_AUTO_TEST_CASE(construct_with_absolute_prefix_)
    {
        fs::path path;
        {
            fs::temporary_directory d { data_dir() / "foo/bar" };
            path = d.path().c_str();

            BOOST_REQUIRE(fs::is_directory(path));
            BOOST_REQUIRE(path::is_child_of(path.c_str(), (data_dir() / "foo").c_str()));

            auto rel = fs::relative(path, data_dir());

            BOOST_TEST(rel.native().starts_with("foo/bar"));
        }

        BOOST_TEST(!fs::exists(path));
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        fs::temporary_directory d1 { "123" };
        fs::temporary_directory d2 { "456" };

        BOOST_TEST(d1 < d2);
        BOOST_TEST(d2 > d1);
        BOOST_TEST(d1 == d1);
        BOOST_TEST(d1 != d2);
    }

BOOST_AUTO_TEST_SUITE_END() // temporary_directory_

} // namespace testing
