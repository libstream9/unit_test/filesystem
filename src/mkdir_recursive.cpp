#include <stream9/filesystem/mkdir_recursive.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/path/concat.hpp>
#include <stream9/test/scoped_cwd.hpp>

namespace testing {

using fs::mkdir_recursive;
using fs::temporary_directory;
using stream9::test::scoped_cwd;

namespace lx { using namespace stream9::linux; }

using stream9::path::operator/;

BOOST_AUTO_TEST_SUITE(mkdir_recursive_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        try {
            auto n = mkdir_recursive("");
            BOOST_TEST(n == 0);
        }
        catch (...) {
            stream9::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(level_1_)
    {
        temporary_directory root;

        auto p = root.path() / "foo";

        auto n = mkdir_recursive(p);
        BOOST_TEST(n == 1);

        auto o_st = lx::nothrow::stat(p);
        BOOST_REQUIRE(!!o_st);
        BOOST_TEST(lx::is_directory(*o_st));
    }

    BOOST_AUTO_TEST_CASE(level_2_)
    {
        temporary_directory root;

        auto p = root.path() / "foo/bar";

        auto n = mkdir_recursive(p);
        BOOST_TEST(n == 2);

        auto o_st = lx::nothrow::stat(p);
        BOOST_REQUIRE(!!o_st);
        BOOST_TEST(lx::is_directory(*o_st));
    }

    BOOST_AUTO_TEST_CASE(relative_1_)
    {
        temporary_directory root;
        scoped_cwd cwd { root.path() };

        auto p = "foo";

        auto n = mkdir_recursive(p);
        BOOST_TEST(n == 1);

        auto o_st = lx::nothrow::stat(p);
        BOOST_REQUIRE(!!o_st);
        BOOST_TEST(lx::is_directory(*o_st));
    }

    BOOST_AUTO_TEST_CASE(relative_2_)
    {
        temporary_directory root;
        scoped_cwd cwd { root.path() };

        auto p = "foo/bar";

        auto n = mkdir_recursive(p);
        BOOST_TEST(n == 2);

        auto o_st = lx::nothrow::stat(p);
        BOOST_REQUIRE(!!o_st);
        BOOST_TEST(lx::is_directory(*o_st));
    }

BOOST_AUTO_TEST_SUITE_END() // mkdir_recursive_

} // namespace testing
