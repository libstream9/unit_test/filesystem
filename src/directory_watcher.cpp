#include <stream9/filesystem/directory_watcher.hpp>

#include "namespace.hpp"

#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/path/concat.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace json = st9::json;
namespace lx = st9::linux;

using st9::path::operator/;
using st9::string_view;

BOOST_AUTO_TEST_SUITE(directory_watcher_)

    BOOST_AUTO_TEST_CASE(event_handler_1_)
    {
        struct handler : public fs::directory_watcher::event_handler {
            json::array events;

            void file_modified(string_view dirpath, string_view fname)
            {
                events.push_back(json::object {
                    { "type", "file_modified" },
                    { "dirpath", dirpath },
                    { "fname", fname },
                });
            }

            void file_deleted(string_view dirpath, string_view fname)
            {
                events.push_back(json::object {
                    { "type", "file_deleted" },
                    { "dirpath", dirpath },
                    { "fname", fname },
                });
            }

            void directory_disappeared(string_view dirpath)
            {
                events.push_back(json::object {
                    { "type", "directory_disappeared" },
                    { "dirpath", dirpath },
                });
            }
        } h;

        fs::directory_watcher w;

        st9::string dirpath;
        {
            fs::temporary_directory d;
            dirpath = d.path();

            w.add_watch(d.path(), h);

            st9::ofstream fs { d.path() / "foo" };
            fs.close();
        }

        w.process_events();

        json::array expected {
            json::object {
                { "type", "file_modified" },
                { "dirpath", dirpath },
                { "fname", "foo" },
            },
            json::object {
                { "type", "file_deleted" },
                { "dirpath", dirpath },
                { "fname", "foo" },
            },
            json::object {
                { "type", "directory_disappeared" },
                { "dirpath", dirpath },
            },
        };

        BOOST_TEST(h.events == expected);
        //std::cerr << std::setw(2) << h.events;
    }

BOOST_AUTO_TEST_SUITE_END() // directory_watcher_

} // namespace testing
