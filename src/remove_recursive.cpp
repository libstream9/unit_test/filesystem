#include <stream9/filesystem/remove_recursive.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/cstring_ptr.hpp>
#include <stream9/filesystem/exists.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/linux/open.hpp>
#include <stream9/linux/mkdir.hpp>

namespace testing {

using fs::remove_recursive;
using fs::temporary_directory;
using fs::exists;
using st9::path::operator/;
using st9::cstring_ptr;

namespace lx { using namespace st9::linux; }

static void
create_file(cstring_ptr const& p)
{
    auto fd = lx::open(p, O_CREAT);
}

BOOST_AUTO_TEST_SUITE(remove_recursive_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        BOOST_CHECK_THROW(remove_recursive(""), stream9::error);
    }

    BOOST_AUTO_TEST_CASE(level_1_1_)
    {
        temporary_directory root;

        auto p = root.path() / "foo";

        create_file(p);

        BOOST_TEST(exists(p));
        remove_recursive(p);
        BOOST_TEST(!exists(p));
    }

    BOOST_AUTO_TEST_CASE(level_1_2_)
    {
        temporary_directory root;

        auto p = root.path() / "foo";

        lx::mkdir(p);

        BOOST_TEST(exists(p));
        remove_recursive(p);
        BOOST_TEST(!exists(p));
    }

    BOOST_AUTO_TEST_CASE(level_2_1_)
    {
        temporary_directory root;

        auto dir = root.path() / "foo";

        lx::mkdir(dir);

        auto p = dir / "bar";
        create_file(p);

        BOOST_TEST(exists(p));
        remove_recursive(dir);
        BOOST_TEST(!exists(p));
        BOOST_TEST(!exists(dir));
    }

    BOOST_AUTO_TEST_CASE(level_2_2_)
    {
        temporary_directory root;

        auto dir = root.path() / "foo";

        lx::mkdir(dir);

        auto p1 = dir / "bar";
        create_file(p1);
        auto p2 = dir / "baz";
        create_file(p2);

        BOOST_TEST(exists(p1));
        BOOST_TEST(exists(p2));
        remove_recursive(dir);
        BOOST_TEST(!exists(p1));
        BOOST_TEST(!exists(p2));
        BOOST_TEST(!exists(dir));
    }

    BOOST_AUTO_TEST_CASE(level_3_1_)
    {
        temporary_directory root;

        auto dir1 = root.path() / "dir1";
        lx::mkdir(dir1);

        auto dir2 = dir1 / "dir2";
        lx::mkdir(dir2);

        auto p1 = dir1 / "bar";
        create_file(p1);
        auto p2 = dir1 / "baz";
        create_file(p2);

        BOOST_TEST(exists(dir1));
        BOOST_TEST(exists(dir2));
        BOOST_TEST(exists(p1));
        BOOST_TEST(exists(p2));
        remove_recursive(dir1);
        BOOST_TEST(!exists(p1));
        BOOST_TEST(!exists(p2));
        BOOST_TEST(!exists(dir1));
        BOOST_TEST(!exists(dir2));
    }

BOOST_AUTO_TEST_SUITE_END() // remove_recursive_

} // namespace testing
