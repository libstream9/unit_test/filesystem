#include <stream9/filesystem/fstream.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/load_string.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/filesystem/temporary_file.hpp>
#include <stream9/filesystem/tmpfstream.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/open.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/string.hpp>

namespace testing {

using stream9::ifstream;
using stream9::ofstream;
using stream9::fstream;
using stream9::linux::open;
using stream9::linux::unlink;
using stream9::string;

using st9::path::operator/;

namespace lx = stream9::linux;

BOOST_AUTO_TEST_SUITE(basic_ifstream_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        try {
            ifstream fs;

            BOOST_TEST(fs.good());
            BOOST_TEST(!fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        try {
            fs::tmpfstream tmp;
            tmp << "foo" << std::flush;

            ifstream fs { tmp.path() };

            BOOST_TEST(fs.good());
            BOOST_TEST(fs.is_open());

            string s;
            fs >> s;

            BOOST_TEST(s == "foo");
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(move_constructor_)
    {
        try {
            fs::tmpfstream tmp;
            tmp << "foo" << std::flush;

            ifstream fs1 { tmp.path() };

            BOOST_TEST(fs1.good());
            BOOST_TEST(fs1.is_open());

            auto fs2 = std::move(fs1);

            BOOST_TEST(fs1.good());
            BOOST_TEST(!fs1.is_open());
            BOOST_TEST(fs2.good());
            BOOST_TEST(fs2.is_open());

            string s;
            fs2 >> s;

            BOOST_TEST(s == "foo");
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(open_1_)
    {
        try {
            fs::tmpfstream tmp;
            tmp << "foo" << std::flush;

            ifstream fs;

            fs.open(tmp.path());

            BOOST_TEST(fs.good());
            BOOST_TEST(fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(close_)
    {
        try {
            fs::tmpfstream tmp;
            tmp << "foo" << std::flush;

            ifstream fs { tmp.path() };

            fs.close();

            BOOST_TEST(fs.good());
            BOOST_TEST(!fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // basic_ifstream_

BOOST_AUTO_TEST_SUITE(ofstream_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = ofstream;

        static_assert(!std::is_copy_constructible_v<T>);
        static_assert(std::is_move_constructible_v<T>);
        static_assert(std::is_nothrow_move_constructible_v<T>);
        static_assert(std::is_move_assignable_v<T>);
        static_assert(std::is_nothrow_move_assignable_v<T>);
        static_assert(std::is_swappable_v<T>);
        static_assert(std::is_nothrow_swappable_v<T>);
    }

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        ofstream s { "foo" };
        unlink("foo");
        BOOST_TEST(s.is_open());
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        auto fd = open("/tmp", O_RDWR | O_TMPFILE);

        ofstream s { std::move(fd) };

        BOOST_TEST(s.is_open());

        s << "foo";
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        try {
            ofstream fs;

            BOOST_TEST(fs.good());
            BOOST_TEST(!fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";
            ::umask(0022);

            ofstream fs { path };

            fs << "foo" << std::flush;

            BOOST_TEST(fs.good());
            BOOST_TEST(fs.is_open());

            auto st = lx::stat(path);
            auto p = st.st_mode & 0777;
            BOOST_TEST(static_cast<unsigned int>(p) == 0644);

            std::string s = fs::load_string(path);
            BOOST_TEST(s == "foo");
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(move_constructor_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";

            ofstream fs1 { path };

            BOOST_TEST(fs1.good());
            BOOST_TEST(fs1.is_open());

            auto fs2 = std::move(fs1);

            BOOST_TEST(fs1.good());
            BOOST_TEST(!fs1.is_open());
            BOOST_TEST(fs2.good());
            BOOST_TEST(fs2.is_open());

            fs2 << "foo" << std::flush;

            BOOST_TEST(fs2.good());
            BOOST_TEST(fs2.is_open());

            std::string s = fs::load_string(path);
            BOOST_TEST(s == "foo");
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(constructor_4_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";

            auto fd = lx::open(path, O_WRONLY | O_CREAT);
            ofstream fs { std::move(fd) };

            BOOST_TEST(fs.good());
            BOOST_TEST(fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(open_1_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";
            ::umask(0022);

            ofstream fs;
            fs.open(path);

            BOOST_TEST(fs.good());
            BOOST_TEST(fs.is_open());

            auto st = lx::stat(path);
            auto p = st.st_mode & 0777;
            BOOST_TEST(static_cast<unsigned int>(p) == 0644);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(close_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";

            ofstream fs { path };

            fs.close();

            BOOST_TEST(fs.good());
            BOOST_TEST(!fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(stream_into_fd_)
    {
        fs::temporary_file tmp;

        tmp.fd() << "bar";

        auto s = fs::load_string(tmp.path());

        BOOST_TEST(s == "bar");
    }

BOOST_AUTO_TEST_SUITE_END() // ofstream_

BOOST_AUTO_TEST_SUITE(basic_fstream_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        try {
            fstream fs;

            BOOST_TEST(fs.good());
            BOOST_TEST(!fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";
            ::umask(0022);

            std::ofstream fs1 { path.c_str() };
            fs1 << "bar";
            fs1.close();

            fstream fs2 { path };
            BOOST_TEST(fs2.good());
            BOOST_TEST(fs2.is_open());

            fs2 << "foo" << std::flush;
            BOOST_TEST(fs2.tellg() == 3);
            BOOST_TEST(fs2.tellp() == 3);
            BOOST_TEST(fs2.good());

            string s1;
            fs2.seekg(0);
            fs2 >> s1;
            BOOST_TEST(s1 == "foo");

            BOOST_TEST(fs2.eof());
            BOOST_TEST(fs2.is_open());

            auto st = lx::stat(path);
            auto p = st.st_mode & 0777;
            BOOST_TEST(static_cast<unsigned int>(p) == 0644);

            string s2 = fs::load_string(path);
            BOOST_TEST(s2 == "foo");
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(constructor_4_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";

            auto fd = lx::open(path, O_RDWR | O_CREAT);
            fstream fs { std::move(fd) };

            BOOST_TEST(fs.good());
            BOOST_TEST(fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(move_constructor_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";

            fstream fs1 { path, fstream::trunc };
            BOOST_TEST(fs1.good());
            BOOST_TEST(fs1.is_open());

            fs1 << "foo" << std::flush;
            BOOST_TEST(fs1.tellg() == 3);
            BOOST_TEST(fs1.tellp() == 3);
            BOOST_TEST(fs1.good());

            auto fs2 = std::move(fs1);

            BOOST_TEST(fs1.good());
            BOOST_TEST(!fs1.is_open());
            BOOST_TEST(fs2.good());
            BOOST_TEST(fs2.is_open());

            BOOST_TEST(fs2.tellg() == 3);
            BOOST_TEST(fs2.tellp() == 3);
            BOOST_TEST(fs2.good());

            string s1;
            fs2.seekg(0);
            fs2 >> s1;
            BOOST_TEST(s1 == "foo");

            BOOST_TEST(fs2.eof());
            BOOST_TEST(fs2.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(open_1_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";
            ::umask(0022);

            ofstream fs1 { path };
            fs1.close();

            fstream fs2;
            fs2.open(path);

            BOOST_TEST(fs2.good());
            BOOST_TEST(fs2.is_open());

            auto st = lx::stat(path);
            auto p = st.st_mode & 0777;
            BOOST_TEST(static_cast<unsigned int>(p) == 0644);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(close_)
    {
        try {
            fs::temporary_directory dir;
            auto path = dir.path() / "foo.txt";

            fstream fs { path, fstream::trunc };

            fs.close();

            BOOST_TEST(fs.good());
            BOOST_TEST(!fs.is_open());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // basic_fstream_
} // namespace testing
