#ifndef STREAM9_FILESYSTEM_TEST_SRC_NAMESPACE_HPP
#define STREAM9_FILESYSTEM_TEST_SRC_NAMESPACE_HPP

namespace stream9::filesystem {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace fs { using namespace st9::filesystem; }

} // namespace testing

#endif // STREAM9_FILESYSTEM_TEST_SRC_NAMESPACE_HPP
